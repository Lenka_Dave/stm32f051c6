#ifndef LED_H_
#define LED_H_
#include "stm32f0xx.h"


void led_init(void);

void blink_led(void);

#endif