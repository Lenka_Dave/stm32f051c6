# Bare Metal STM32F051C6

This is bare metal programming repository for UCT second year development board
Everything is written from sratch

## Start up code is written in C
## Linker Script and Make file 
## MCU specific header files but not all peripherals
## LCD library

# Getting Started STM32F0xx series

Clone the Repository to your local computer,, Go to linker script src/stm32_ls.ld
change RAM and Flash size according to your processor specs...Other than that Everything is the same. Note for UCT board you do not need to change anything.



# Tools needed
1. Arm GNU toolchain
2. OpenOcd
3. Make

# ToolChain Installation
1. Go to https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads and down the toolchain, if you are in linux (ubuntu) you use command: sudo apt-get install gcc-arm-none-eabi

2. Download the software and after installation do this:

Windows; arm-none-eabi installation page; go to bin and add that path to Enviroment variables
In the installation folder go to arn-none-eabi/lib and add it to enviroment variables

Then go to terminal or command prompt and type: arm-none-eabi-gcc  if everything was successful you will get the message: 
arm-none-eabi-gcc: fatal error: no input files
compilation terminated.

For linux go to terminal and type nano~/.bashrc    and at the end of the file add the paths mentioned for windows   


#O penocd
Linux ; sudo apt install openocd
Windows go to https://gnutoolchains.com/arm-eabi/openocd/ and extract the file to your project folder and add bin folder to path
Then go to terminal and type openocd, you will get version info and that is installation is sucessful
# Make
For linux: sudo apt install make

Windows: https://gnuwin32.sourceforge.net/packages/make.htm    download setup and install and then add bin path to enviroment variables
type make --version on terminal/command prompt if installation is succesful you will get version info or download the zipped file extract it to your project folder and add bin to path



# Downloading Code to our Board

1. go to project folder contaaining Makefile
2. open that folder in terminal
3. type make
4. if no errors type command : make load
5. Open new terminal/command prompt window
6. type: arm-none-eabi-gdb
7. enter command: target extended-remote localhost:3333  # port number will be seen on message you get after make load
8. enter: monitor reset init
9. enter: monitor flash write_image erase final.elf
10. enter: monitor reset halt
11. enter monitor resumE

# For UCT Devboard

User LED0 will blink for every second
You will see messsage on LCD screen
## "Hello World!!"
## "Welcome Done!!"
Press SW0 led1 will turn on


### notes.txt file has some useful notes
### To do some depth info on installation library usage and more .......







